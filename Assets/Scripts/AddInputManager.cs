using UnityEngine;
using UnityEngine.UI;

public class AddInputManager : MonoBehaviour
{
    [SerializeField] Button m_addButton = null;
    [SerializeField] Button m_exitButton = null;

    [SerializeField] Text m_inputName = null;
    [SerializeField] Text m_inputScore = null;

    private string m_nametxt = null;
    private int m_scoretxt;
    private void Awake()
    {
        m_exitButton.onClick.AddListener(() => ClosePanel());
        m_addButton.onClick.AddListener(() => AddProfile());
        ClosePanel();
    }

    public void AddProfile()
    {
        m_nametxt = m_inputName.text;
        m_scoretxt = int.Parse(m_inputScore.text);
        LeaderBoardTable.Instance.AddProfile(m_scoretxt, m_nametxt);
        ClosePanel();
    }
    public void ClosePanel()
    {
        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
        else { return; }
    }
}
