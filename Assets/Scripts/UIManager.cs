using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private AddInputManager m_AddPanel;
    [SerializeField] private EditProfileManager m_EditPanel;

    [SerializeField] Button m_addButton = null;

    private void Awake()
    {
        m_addButton.onClick.AddListener(() => { OpenAddPanel(); CheckButton(); });  //  TODO ����� ������� ������ ����������� ����� ������� ����� 
    }
    public void OpenAddPanel()
    {
        if(!m_AddPanel.gameObject.activeInHierarchy)
            m_AddPanel.gameObject.SetActive(true);       
    }

    public void OpenEditPanel()
    {
        if (!m_EditPanel.gameObject.activeInHierarchy)
        m_EditPanel.gameObject.SetActive(true);
    }
    public void CheckButton()
    {
        Debug.Log("Pushed");
    }
}
