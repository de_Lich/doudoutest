using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.EventSystems;

public class LeaderBoardTable : MonoBehaviour
{
    #region Singleton Init
    private static LeaderBoardTable instance;

    void Awake()
    {
        if (instance == null)
            Init();
        else if (instance != this)
            Destroy(gameObject);
    }

    public static LeaderBoardTable Instance 
    {
        get
        {
            if (instance == null)
                Init();
            return instance;
        }
        private set { instance = value; }
    }

    static void Init()
    {
        instance = FindObjectOfType<LeaderBoardTable>();
        if (instance != null)
            instance.Initialize();
    }
    #endregion
    const string LeaderBoard = "leaderboard";

    private Transform m_scoreContainer;
    private Transform m_scoreEntryTemplate;
    [SerializeField] private List<Transform> m_profileEntryList;


    private void Initialize()
    {
        m_scoreContainer = transform.Find("scoreContainer");
        m_scoreEntryTemplate = transform.Find("scoreEntryTemplate");

        m_scoreEntryTemplate.gameObject.SetActive(false);

        string jsonString = PlayerPrefs.GetString(LeaderBoard);
        ProfileScore profileScore = JsonUtility.FromJson<ProfileScore>(jsonString);
        SetLeaderboard(profileScore);
    }

    // ������� ������ ��������
    public void SetLeaderboard(ProfileScore profileScore)
    {
        SortProfileScore(profileScore);
        m_profileEntryList = new List<Transform>();
        foreach (ProfileEntry profile in profileScore.profileList)
        {
            CreateLeaderboardEntryTransform(profile, m_scoreContainer, m_profileEntryList);
        }
    }

    private void SortProfileScore(ProfileScore profileScore)
    {
        for (int i = 0; i < profileScore.profileList.Count; i++)                        // ����� ������� ������������ Linq,
        {                                                                               // �� �� ���� ��������� ��� �������� ������ � profileList.score,
            for (int j = i + 1; j < profileScore.profileList.Count; j++)                // � ������������ ��������� ��������� ����� ������
            {
                if (profileScore.profileList[j].score > profileScore.profileList[i].score)
                {
                    ProfileEntry tmp = profileScore.profileList[i];
                    profileScore.profileList[i] = profileScore.profileList[j];
                    profileScore.profileList[j] = tmp;
                }
            }
        }
    }

    // ������� ������� � ��� ��������� � �������
    private void CreateLeaderboardEntryTransform(ProfileEntry profileEntry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 30f;
        Transform entryTransform = Instantiate(m_scoreEntryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, templateHeight * transformList.Count); 
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;
        int score = profileEntry.score;
        string name = profileEntry.name;
        entryTransform.Find("Positiontxt").GetComponent<Text>().text = rank.ToString() ;
        entryTransform.Find("Nametxt").GetComponent<Text>().text = name;
        entryTransform.Find("Scoretxt").GetComponent<Text>().text = score.ToString();

        transformList.Add(entryTransform);
    }

    // ��������� ����� �������
    public void AddProfile(int score, string name)
    {
        ProfileEntry profileEntry = new ProfileEntry { score = score, name = name };
        string jsonString = PlayerPrefs.GetString(LeaderBoard);
        ProfileScore profileScore = JsonUtility.FromJson<ProfileScore>(jsonString);

        profileScore.profileList.Add(profileEntry);
        SaveToJson(profileScore);
        SortProfileScore(profileScore);
        foreach (var profile in m_profileEntryList)
        {
            DestroyOldList(profile);
        }
        SetLeaderboard(profileScore);
    }

    // ������� ������ ������
    private void DestroyOldList(Transform transform)
    {
        Destroy(transform.gameObject);
    }

    // ������� ��������� �������
    public void RemoveProfile(ProfileEntry profileEntry)                                             
    {
        string jsonString = PlayerPrefs.GetString(LeaderBoard);
        ProfileScore profileScore = JsonUtility.FromJson<ProfileScore>(jsonString);

        profileScore.profileList.Remove(profileEntry);
        SaveToJson(profileScore);
        SortProfileScore(profileScore);             // refactor doublecode later
        foreach (var profile in m_profileEntryList)
        {
            DestroyOldList(profile);
        }
        SetLeaderboard(profileScore);
    }

    public void EditProfile(ProfileEntry profile)
    {
        
    }
    public void OnSelect(BaseEventData eventData)
    {
        if (EventSystem.current.currentSelectedGameObject == m_scoreEntryTemplate)
        {
            Debug.Log(m_scoreEntryTemplate.name + " was selected");
        }
    }
    //��������� ������� ��������
    private static void SaveToJson(ProfileScore profileScore)
    {
        string json = JsonUtility.ToJson(profileScore);
        PlayerPrefs.SetString(LeaderBoard, json);
        PlayerPrefs.Save();
    }

    // ���� ��������
    public class ProfileScore
    {
        //public Dictionary<ProfileEntry, int> profiles;
        public List<ProfileEntry> profileList; 
    }

    //  ������ ������� ������
    [System.Serializable]
    public class ProfileEntry
    {
        public int score;
        public string name;
    }

    private void Update()
    {
        if (EventSystem.current == m_scoreEntryTemplate)
        {
            Debug.Log(m_scoreEntryTemplate.name + "was selected");
        }
    }
}
