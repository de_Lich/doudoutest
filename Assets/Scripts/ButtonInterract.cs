using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonInterract : Selectable, ISelectHandler
{
    public void OnSelect(BaseEventData eventData)
    {
        if (EventSystem.current == gameObject)
        {
            Debug.Log(gameObject.name + " was selected");
        }
    }
}
