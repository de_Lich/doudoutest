using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventHandler : MonoBehaviour
{
    public void Update()
    {
        // Compare selected gameObject with referenced Button gameObject
        if (EventSystem.current == gameObject)
        {
            Debug.Log(this.gameObject.name + " was selected");
        }
    }
}
