using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditProfileManager : MonoBehaviour
{
    [SerializeField] Button m_editButton = null;
    [SerializeField] Button m_exitButton = null;

    [SerializeField] Text m_inputName = null;
    [SerializeField] Text m_inputScore = null;
    
    private string m_nametxt = null;
    private int m_scoretxt;

    EventSystem m_eventSelected;
    GameObject currentSelectedGameObject;
    private void Awake()
    {
        m_exitButton.onClick.AddListener(() => ClosePanel());
        m_editButton.onClick.AddListener(() => EditProfile(currentSelectedGameObject));

        currentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
        //m_eventSelected = currentSelectedGameObject;
        ClosePanel();
    }

    private void EditProfile(GameObject profile)
    {
        if (profile != null)
        {
            //m_nametxt = m_inputName.text;
            //m_scoretxt = int.Parse(m_inputScore.text);
            //LeaderBoardTable.Instance.AddProfile(m_scoretxt, m_nametxt);
            Debug.Log(m_eventSelected.name + "yes beach!");
        }
        ClosePanel();
    }

    public void ClosePanel()
    {
        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);
        else { return; }
    }
}
